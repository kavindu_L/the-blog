import React from "react";
import ReactDOM from "react-dom/client";
import TheBlog from "./TheBlog";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <TheBlog />
  </React.StrictMode>
);
