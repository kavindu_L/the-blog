import { Link } from "react-router-dom";

function NavBar({ darkMode }) {
  return (
    <div>
      <nav className="nav flex-column">
        <Link className="nav-link" to="/">
          <h4>Home</h4>
        </Link>
        <Link className="nav-link" to="/about">
          <h4>About</h4>
        </Link>
        <Link className="nav-link" to="/contact">
          <h4>Contact Us</h4>
        </Link>
      </nav>
    </div>
  );
}

export default NavBar;
