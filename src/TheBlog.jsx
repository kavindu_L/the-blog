import NavBar from "./NavBar";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import Post from "./pages/Post";
import About from "./pages/About";
import ContactUs from "./pages/ContactUs";
import { useState } from "react";
import { AiOutlineCopyrightCircle } from "react-icons/ai";

function TheBlog() {
  const [darkMode, setDarkMode] = useState(() => {
    const savedItem = sessionStorage.getItem("darkMode");
    const parsedItem = JSON.parse(savedItem);
    return parsedItem || false;
  });

  sessionStorage.setItem("darkMode", JSON.stringify(darkMode));

  return (
    <BrowserRouter basename={process.env.PUBLIC_URL}>
      <div className="container mt-3 w-75">
        <h1 className="display-4">
          <strong>The Blog</strong>
        </h1>
        <div className="d-flex justify-content-between mt-4">
          <div
            className={
              "align-self-start shadow me-3 rounded bg-" +
              (darkMode ? "dark" : "light")
            }
            // style={{ height: "800px" }}
          >
            <NavBar darkMode={darkMode} />
          </div>

          <div className="rounded w-75 me-3">
            <div>
              <Routes>
                <Route path="/" element={<Home darkMode={darkMode} />} />
                <Route path="/:id" element={<Post darkMode={darkMode} />} />
                <Route path="/about" element={<About darkMode={darkMode} />} />
                <Route
                  path="/contact"
                  element={<ContactUs darkMode={darkMode} />}
                />
              </Routes>
            </div>
          </div>

          <div className="form-check form-switch">
            <input
              className="form-check-input"
              type="checkbox"
              role="switch"
              id="flexSwitchCheckDefault"
              onChange={() => setDarkMode(!darkMode)}
            />
            <label
              className="form-check-label"
              htmlFor="flexSwitchCheckDefault"
            >
              Dark mode
            </label>
          </div>
        </div>

        <div className="text-center mt-4 opacity-25">
          <AiOutlineCopyrightCircle />{" "}
          <span>
            <i>2022 @ The Blog All rights reserved</i>
          </span>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default TheBlog;
