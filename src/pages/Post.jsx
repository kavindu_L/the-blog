import { useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import Comments from "./Comments";
// import { BsHeartFill, BsHeart } from "react-icons/bs";

function Post({ darkMode, test }) {
  const { id } = useParams();
  const [post, setPost] = useState({});

  // const [like, setLike] = useState(false);
  // const [like, setLike] = useState(() => {
  //   const savedItem = localStorage.getItem("isLiked");
  //   const parsedItem = JSON.parse(savedItem);
  //   return parsedItem || false;
  // });

  // localStorage.setItem("isLiked", JSON.stringify(like));

  // function handleClick(event) {
  //   console.log(event.target.color);
  //   setLike(!like);
  // }

  useEffect(() => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
      .then(res => res.json())
      .then(data => setPost(data))
      .catch(err => console.log(err));
  }, [id]);

  return (
    <>
      {!post.id ? (
        <div className="spinner-border" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
      ) : (
        <div className="d-flex">
          <div className="overflow-auto" style={{ height: "800px" }}>
            <div
              className={
                "container shadow p-3 rounded align-self-start bg-" +
                (darkMode ? "dark text-white" : "light text-dark")
              }
            >
              <div className="d-flex justify-content-between align-items-center">
                <div>
                  <h2 className="mb-3">
                    <strong>{post.title}</strong>
                  </h2>
                </div>
                {/* <div onClick={handleClick}>
          {!like ? (
            <BsHeart size="25px" />
          ) : (
            <BsHeartFill color="red" size="25px" />
          )}
        </div> */}
              </div>

              <p>{post.body}</p>
            </div>
          </div>

          <Comments id={id} darkMode={darkMode} />
        </div>
      )}
    </>
  );
}

export default Post;
