import { Link } from "react-router-dom";

function About({ darkMode }) {
  return (
    <div className="container overflow-auto" style={{ height: "800px" }}>
      <div
        className={
          "shadow p-3 rounded bg-" +
          (darkMode ? "dark text-white" : "light text-dark")
        }
      >
        <h2>About</h2>
        <p>
          Hello, Dear friends, Welcome to The Blog also, we are happy you want
          to know something more about our site
        </p>
        <p>
          So, basically, nowadays people are more dependent on online products
          and services that's why we also, take forward a step to help you.
        </p>
        <p>
          Our first wish is to provide you with a better solution to solve your
          problem. So, kindly if you don't get any solution then mention it in
          the comment section.
        </p>
        <p>
          Also, we are trying to provide fresh & latest content that provides
          you ideas about all updated information that's happening in the world.
        </p>
        <p>
          In the below section you can get more ideas about our site like our
          website category and content category.
        </p>
        <p>
          If you have additional questions or require more information about our
          About Us Page, do not hesitate to contact us through email at{" "}
          <Link to="/contact" className="text-primary">
            theblog@email.com
          </Link>
        </p>
        <h2>
          <b>What is Our Goal?</b>
        </h2>
        <p>
          There are millions of websites created every day, also, there is much
          fake content spread on the internet.
        </p>
        <p>
          So, Our main goal is to provide you with 100% Original and Safe
          content that provides you a great and better experience on the world
          wide web.
        </p>
        <p>
          We mainly focus on our service so and improving it regularly to
          provide a better user experience to all users.
        </p>
        <p>
          Basically, we focus on the Blogging niche so, our main priority is to
          search for new content and present it in front of you to learn
          something new.
        </p>
        <h2>
          <b>What is our Service?</b>
        </h2>
        <p>
          We are mainly focused on the Blogging category so, we provide Blogging
          related content if you are interested in the Blogging category then
          you can visit daily to get more latest information.
        </p>
        <p>
          On our website The Blog you get can all Blogging related information
          also, we focus on many other categories and we hope you like also, the
          content of other categories that are maintained on our website. So,
          you can visit our website homepage to know all category details here
          you can visit our homepage Click here -->
          <Link to="/" className="text-primary">
            The Blog
          </Link>
        </p>
        <p>
          Also, we provide a Notification update service you can join by email
          and other Social Media Platforms, and all Links you can get on the
          Homepage visit now.{" "}
          <Link to="/" className="text-primary">
            The Blog
          </Link>
        </p>

        <center>
          <p>"Thanks for visiting our About Us Page"</p>
        </center>
      </div>
    </div>
  );
}

export default About;
