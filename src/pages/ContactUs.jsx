import { useRef } from "react";

function ContactUs({ darkMode }) {
  let emailAddress = useRef();
  let emailSubject = useRef();
  let emailBody = useRef();

  function handleSubmit(event) {
    event.preventDefault();
    emailAddress.current.value = "";
    emailSubject.current.value = "";
    emailBody.current.value = "";

    alert("Email sent ! Thanks for contacting us");
  }

  return (
    <div className="container overflow-auto" style={{ height: "800px" }}>
      <div
        className={
          "shadow p-3 rounded bg-" +
          (darkMode ? "dark text-white" : "light text-dark")
        }
      >
        <h2>Contact Us</h2>
        <form onSubmit={handleSubmit}>
          <div className="mt-3 mb-3">
            <label htmlFor="exampleFormControlInput1" className="form-label">
              Email address
            </label>
            <input
              ref={emailAddress}
              type="email"
              className="form-control"
              id="exampleFormControlInput1"
              placeholder="name@example.com"
            />
          </div>
          <div className="mt-3 mb-3">
            <label htmlFor="exampleFormControlInput2" className="form-label">
              Email Subject
            </label>
            <input
              ref={emailSubject}
              type="text"
              className="form-control"
              id="exampleFormControlInput2"
            />
          </div>
          <div className="mb-3">
            <label htmlFor="exampleFormControlTextarea1" className="form-label">
              Email Body
            </label>
            <textarea
              ref={emailBody}
              className="form-control"
              id="exampleFormControlTextarea1"
              rows="3"
            ></textarea>
          </div>
          <div className="d-flex justify-content-center">
            <input
              type="submit"
              className="btn btn-primary"
              value="Send Email"
            />
          </div>
        </form>
      </div>
    </div>
  );
}

export default ContactUs;
