import { useState, useEffect } from "react";

function Comments({ id, darkMode }) {
  const [comments, setComments] = useState([]);

  useEffect(() => {
    fetch(`https://jsonplaceholder.typicode.com/comments?postId=${id}`)
      .then(res => res.json())
      .then(data => setComments(data))
      .catch(err => console.log(err));
  }, [id]);

  return (
    <div className="container overflow-auto" style={{ height: "800px" }}>
      <div className="container">
        <h3
          className={
            "sticky-top rounded ps-3 bg-" +
            (darkMode ? "dark text-white" : "light text-dark")
          }
        >
          Comments
        </h3>

        {!comments.length ? (
          <div className="spinner-border" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        ) : (
          comments.map(comment => (
            <div
              className={
                "mb-3 p-3 shadow rounded bg-" +
                (darkMode ? "dark text-white" : "light text-dark")
              }
              key={comment.id}
            >
              <p>
                <span className="text-primary">
                  @{comment.email.split("@")[0]}
                </span>{" "}
                {comment.body}
              </p>
            </div>
          ))
        )}
      </div>
    </div>
  );
}

export default Comments;
