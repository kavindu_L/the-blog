import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

function Home({ darkMode }) {
  const [posts, setPosts] = useState(() => {
    const savedItem = sessionStorage.getItem("posts");
    const parsedItem = JSON.parse(savedItem);
    return parsedItem || [];
  });
  // const [like, setLike] = useState(false);

  sessionStorage.setItem("posts", JSON.stringify(posts));

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/posts")
      .then(res => res.json())
      .then(data => setPosts(data))
      .catch(err => console.log(err));
  }, []);

  return (
    <div className="container overflow-auto" style={{ height: "800px" }}>
      {/* <h2>Home</h2> */}
      <div>
        {posts.length ? (
          posts.map(post => (
            <div
              className={
                "card shadow rounded mb-3 bg-" +
                (darkMode ? "dark text-white" : "light text-dark")
              }
              style={{ border: "none" }}
              key={post.id}
            >
              <div className="card-body" id={post.id}>
                <h5 className="card-title">{post.title}</h5>
                <p className="card-text">
                  {post.body.substring(0, 70) + "..."}
                </p>
                <Link to={`/${post.id}`} className="card-link">
                  <i>see more</i>
                </Link>

                {/* <div className="d-flex justify-content-between">
                  <div>
                    <Link to={`/${post.id}`} className="card-link">
                      <i>see more</i>
                    </Link>
                  </div>
                  <div>
                    <button
                      className="rounded btn btn-light"
                      onClick={e => {
                        console.log(
                          e.target.className.split(" ")[2] === "btn-light"
                        );

                        if (e.target.className.split(" ")[2] === "btn-light") {
                          e.target.className = "rounded btn btn-danger";
                        } else {
                          e.target.className = "rounded btn btn-light";
                        }
                      }}
                    >
                      like
                    </button>
                  </div>
                </div> */}
              </div>
            </div>

            // <div className="card mb-3" style={{ width: "18rem" }}>
            //   <img
            //     src="https://picsum.photos/200/300"
            //     className="card-img-top"
            //     alt={post.id}
            //   />
            //   <div className="card-body">
            //     <h5 className="card-title">{post.title}</h5>
            //     <p className="card-text">
            //       {post.body.substring(0, 70) + "..."}
            //     </p>
            //     <Link to={`/${post.id}`} className="btn btn-primary">
            //       <i>see more</i>
            //     </Link>
            //   </div>
            // </div>
          ))
        ) : (
          <div className="spinner-border" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        )}
      </div>
    </div>
  );
}

export default Home;
